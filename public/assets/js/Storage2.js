var banana = 20;
var apple = 50;
var durian = 30;
var mango = 60;

function CheckBrowser() {
    if ('localStorage' in window && window['localStorage'] !== null) {

        return true;
    } else {
            return false;
    }
}

function doShowAll() {
    if (CheckBrowser()) {
        var key = "";
        var list = "<tr><th>Item</th><th>Quantity</th><th>price</th></tr>\n";
        var i = 0;

        
        for (i = 0; i <= localStorage.length-1; i++) {
            key = localStorage.key(i);

            list += "<tr><td>" + key + "</td>\n<td>"
                    + localStorage.getItem(key) + "</td>\n<td>"
                    + banana * localStorage.getItem(key)  + "</td></tr>\n";
        }
     
        if (list == "<tr><th>Item</th><th>quantity</th></tr>\n") {
            list += "<tr><td><i>empty</i></td>\n<td><i>empty</i></td></tr>\n";
        }

        document.getElementById('lists').innerHTML = list;
    } else {
        alert('Cannot save shopping list as your browser does not support HTML 5');
    }
}

function SaveItem() {

    var first = document.forms.ShoppingList.first.value;
    var last = document.forms.ShoppingList.last.value;
     localStorage.setItem(first,last);

    var birth = document.forms.ShoppingList.birth.value;
    var addrs = document.forms.ShoppingList.addrs.value;
     localStorage.setItem(birth, addrs);

    var phone = document.forms.ShoppingList.phone.value;
    var email = document.forms.ShoppingList.email.value;
     localStorage.setItem(phone, email);

    doShowAll();

}

function ClearAll() {
    localStorage.clear();
    doShowAll();
}

function order(){

  var fullname = window.localStorage.getItem('name');
  var address = window.localStorage.getItem('address');
  var email = window.localStorage.getItem('email');
 
  document.getElementById("first").innerHTML = "full name: " + fullname;
  document.getElementById("address").innerHTML = "Address: " + address;
  document.getElementById("email").innerHTML = "Email: " + email;

  var hee = JSON.parse(window.localStorage.getItem('banana'));
  var yoo = JSON.parse(window.localStorage.getItem('apple'));
  var woo = JSON.parse(window.localStorage.getItem('durian'));
  var loo = JSON.parse(window.localStorage.getItem('mango'));

  document.getElementById("demo1").innerHTML = "mango price: 60" + " " + "total: " + (loo*mango);
  document.getElementById("demo2").innerHTML = "durian price: 30" + " " + "total: " + (woo*durian);
  document.getElementById("demo3").innerHTML = "apple price: 50" + " " + "total: " + (yoo*apple);
  document.getElementById("demo4").innerHTML = "banana price: 20" + " " + "total: " + (hee*banana);
  document.getElementById("demo").innerHTML = "Sub Total" + " " + ((hee * banana) + (yoo * apple) + (woo * durian) + (loo * mango));
}

function on() {
  document.getElementById("overlay").style.display = "block";
}

function off() {
  document.getElementById("overlay").style.display = "none";
}

