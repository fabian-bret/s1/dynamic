var banana = 20;
var apple = 50;
var durian = 30;
var mango = 60;

/*
=====> Checking browser support.
 //This step might not be required because most modern browsers do support HTML5.
 */
 //Function below might be redundant.

function CheckBrowser() {
    if ('localStorage' in window && window['localStorage'] !== null) {
        // We can use localStorage object to store data.
        return true;
    } else {
            return false;
    }
}


// Dynamically populate the table with shopping list items.
//Step below can be done via PHP and AJAX, too.
function doShowAll() {
    if (CheckBrowser()) {
        var key = "";
        var list = "<tr><th>Item</th><th>Quantity</th></tr>\n";
        var i = 1;
        var hee = JSON.parse(window.localStorage.getItem('Banana'));
        var KeyName = window.localStorage.key(0)
        //For a more advanced feature, you can set a cap on max items in the cart.
        for (i = 0; i <= localStorage.length-1; i++) {
            key = localStorage.key(i);
            list += "<tr><td>" + key + "</td>\n<td>"
                    + localStorage.getItem(key) + "</td>\n</tr>\n";
        }
        //If no item exists in the cart.
        if (list == "<tr><th>Item</th><th>quantity</th></tr>\n") {
            list += "<tr><td><i>empty</i></td>\n<td><i>empty</i></td></tr>\n";
        }
        //Bind the data to HTML table.
        document.getElementById('list').innerHTML = list;
    } else {
        alert('Cannot save shopping list as your browser does not support HTML 5');
    }
}

function SaveItem() {

    var name = document.forms.ShoppingList.name.value;
    var data = document.forms.ShoppingList.data.value;
    localStorage.setItem(name, data);

    doShowAll();

}

//Change an existing key-value in HTML5 storage.
function ModifyItem() {
    var name1 = document.forms.ShoppingList.name.value;
    var data1 = document.forms.ShoppingList.data.value;
    //check if name1 is already exists

//Check if key exists.
            if (localStorage.getItem(name1) !=null)
            {
              //update
              localStorage.setItem(name1,data1);
              document.forms.ShoppingList.data.value = localStorage.getItem(name1);
            }
    myFunction();
    doShowAll();
}


function RemoveItem()
{
var name=document.forms.ShoppingList.name.value;
document.forms.ShoppingList.data.value=localStorage.removeItem(name);
doShowAll();
}


function ClearAll() {
    localStorage.clear();
    doShowAll();
}

function myFunction() {
  var hee = JSON.parse(window.localStorage.getItem('banana'));
  var yoo = JSON.parse(window.localStorage.getItem('apple'));
  var woo = JSON.parse(window.localStorage.getItem('durian'));
  var loo = JSON.parse(window.localStorage.getItem('mango'));
  //var KeyName = window.localStorage.key(0) + window.localStorage.key(1);
  document.getElementById("demo1").innerHTML = "mango price: 60 pesos x" + loo + " " + "total: " + (loo*mango);
  document.getElementById("demo2").innerHTML = "durian price: 30 pesos x" + woo + " " + "total: " + (woo*durian);
  document.getElementById("demo3").innerHTML = "apple price: 50 pesos x" + yoo + " " + "total: " + (yoo*apple);
  document.getElementById("demo4").innerHTML = "banana price: 20 pesos x" + hee + " " + "total: " + (hee*banana);
  document.getElementById("demo").innerHTML = "Sub Total" + " " + ((hee * banana) + (yoo * apple) + (woo * durian) + (loo * mango));
}